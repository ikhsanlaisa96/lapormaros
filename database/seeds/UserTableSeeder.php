<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->username = "developer";
        $user->password = bcrypt("secret");
        $user->no_telp = "01231241231";
        $user->id_penduduk = 1;
        $user->save();
    }
}
