<?php

use Illuminate\Database\Seeder;

class KategoriLaporanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = new \App\KategoriLaporan();
        $kategori -> nama = 'Sampah';
        $kategori ->save();
    }
}
