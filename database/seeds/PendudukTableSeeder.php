<?php

use Illuminate\Database\Seeder;

class PendudukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penduduk = new \App\Penduduk();
        $penduduk-> nik ='34793249';
        $penduduk-> nama ='ikhsan';
        $penduduk-> tgl_lahir ='2017-04-01';
        $penduduk-> alamat ='btis8';
        $penduduk->save();
    }
}
