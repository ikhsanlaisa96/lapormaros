<?php

namespace App\Http\Controllers;

use App\Laporan;
use Illuminate\Http\Request;

class ApiLaporanController extends Controller
{
    function __construct()
    {
        $this->middleware("auth:api");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $laporan = new Laporan();
        $laporan -> judul = $request->input('judul');
        $laporan -> deskripsi = $request->input('deskripsi');
        $laporan -> latitude = $request->input('latitude');
        $laporan -> longitude = $request->input('longitude');
        $laporan -> id_user = $request->input('id_user');
        $laporan -> id_kategori = $request->input('id_kategori');
        $laporan->save();
        return[
            'error'=>false,
            'message'=>'data berhasil'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
