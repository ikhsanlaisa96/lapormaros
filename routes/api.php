<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('user', 'ApiUserController@index')->middleware("auth:api");
Route::post('register', 'ApiUserController@store');
Route::post('check', 'ApiPendudukController@store');
Route::post('laporan', 'ApiLaporanController@store');
Route::post('gambar', 'ApiGambarLaporanController@store');

